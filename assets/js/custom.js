'use strict';
/**
 *   1- Desktop menu
 *   2- Mobile menu
 *   3- Accordion, toggle
 *   4- Carousel
 *   5- Hover div effect
 *   6- Google map
 *   7- Masonry layout
 *   8- Quantity product
 *   9- sync carousel
 *   8- Gallery image flick
 *   9- Slider animation
 *   10- Search bar
 *   11- Validate form
 *   12- Responsive tabs
 *   13- Scroll animation
 *-----------------------------------------------------------------
 **/
var kopa_variable = {
    "contact": {
        "address": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
        "marker": "/url image"
    },
    "i18n": {
        "VIEW": "View",
        "VIEWS": "Views",
        "validate": {
            "form": {
                "SUBMIT": "Submit",
                "SENDING": "Sending..."
            },
            "name": {
                "REQUIRED": "Please enter your name",
                "MINLENGTH": "At least {0} characters required"
            },
            "email": {
                "REQUIRED": "Please enter your email",
                "EMAIL": "Please enter a valid email"
            },
            "url": {
                "REQUIRED": "Please enter your url",
                "URL": "Please enter a valid url"
            },
            "subject": {
                "REQUIRED": "Please enter a subject",
                "MINLENGTH": "At least {0} characters required"
            },
            "message": {
                "REQUIRED": "Please enter a message",
                "MINLENGTH": "At least {0} characters required"
            }
        },
        "tweets": {
            "failed": "Sorry, twitter is currently unavailable for this user.",
            "loading": "Loading tweets..."
        }
    },
    "url": {
        "template_directory_uri":"file://"
    }
};

jQuery( document).ready(function($) {
    k_menu();
    k_mobile_menu();
    k_accordion();
    k_owl_carousel();
    k_hover_div();    
    k_masonry();
    k_quantity();
    k_sync_carousel();
    k_flick();
    k_search_bar();
    k_slider_animation();
    k_map();
    k_validate();
    k_tabs();
    k_scroll_animation();
});

/**
 * @description Desktop menu
 * @return 
 */
function k_menu(){
	if ($('.sf-menu').length > 0) {
		Modernizr.load([{
	        load: ['assets/js/superfish.js'],
	        complete: function () {
	            $('.sf-menu').superfish();
	        }
	    }]);
    }
}

/**
 * @description Mobile menu
 * @return 
 */
function k_mobile_menu(){
	if ($('#mobile-menu').length > 0) {
		Modernizr.load([{
	        load: ['assets/js/mmenu.min.all.js'],
	        complete: function () {
	            $('#mobile-menu').mmenu();
	        }
	    }]);
    }
}

/**
 * @description Accordion, toggle
 * @return 
 */
function k_accordion(){
	$('.k-toggle .panel-heading').click(function(){
		$(this).parent().find('.collapse').collapse('toggle');
		$(this).find('a').toggleClass('collapsed');
	});

	$('.k-accordion .panel-heading').click(function(){	
		
		$(this).closest('.k-accordion').find('.collapse').collapse('hide');
		$(this).parent().find('.collapse').collapse('toggle');		

		$(this).closest('.k-accordion').find('.panel-heading').not(this).find('a').addClass('collapsed');
		$(this).find('a').toggleClass('collapsed');		
	});
}

/**
 * @description Carousel
 * @return 
 */
function k_owl_carousel(){	
	if ($('.owl-carousel').length > 0) {
		Modernizr.load([{
            load: ['assets/js/masonry.js', 'assets/js/owl.carousel.min.js'],
            complete: function () {            
				if ($('.k-widget-logo .owl-carousel').length > 0) {
					$('.k-widget-logo .owl-carousel').owlCarousel({
						items:5,
						slideSpeed:500,
						pagination:false,
						navigation:true,
						navigationText:['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
						
					});
				}

				if ($('.k-widget-related-product .owl-carousel').length > 0) {
					$('.k-widget-related-product .owl-carousel').owlCarousel({
						items:4,
						slideSpeed:500,
						pagination:false,
						navigation:true,
						navigationText:['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
						
					});
				}

				if ($('.k-widget-testi .owl-carousel').length > 0) {
					$('.k-widget-testi .owl-carousel').owlCarousel({
						singleItem:true,
						slideSpeed:500,						
					});
				}

				if ($('.k-widget-post-carousel .owl-carousel').length > 0) {
					$('.k-widget-post-carousel .owl-carousel').owlCarousel({
						items:2,
						itemsDesktop:[1366,2],
						itemsDesktopSmall:[1000,1],
						slideSpeed:500,
						pagination:false,
						navigation:true,
						scrollPerPage:true,
						navigationText:['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
						
					});
				}

				if ($('.single-carousel').length > 0) {
					$('.single-carousel').owlCarousel({
						singleItem:true,
						slideSpeed:500,
						pagination:false,
						navigation:true,
						navigationText:['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
						autoHeight:true,
						afterInit : function(){
							if ($('.list-blog-post-timeline').length > 0) {
								setTimeout(function(){
									$('.list-blog-post-timeline').masonry().masonry('layout');	
								},1000);
							}
							
							if ($('.list-blog-post-masonry').length > 0) {
								setTimeout(function(){
									$('.list-blog-post-masonry').masonry().masonry('layout');	
								},1000);
							}
						},
						afterUpdate : function(){
							if ($('.list-blog-post-timeline').length > 0) {
								setTimeout(function(){
									$('.list-blog-post-timeline').masonry().masonry('layout');	
								},1000);
							}
							
							if ($('.list-blog-post-masonry').length > 0) {
								setTimeout(function(){
									$('.list-blog-post-masonry').masonry().masonry('layout');	
								},1000);
							}
						}
					});
				}
			}
        }]);
	}
}

/**
 * @description Hover div effect
 * @return 
 */
function k_hover_div(){	
	if ($('.hover_div').length > 0) {
		Modernizr.load([{
	        load: ['assets/js/masonry.js', 'assets/js/imagesloaded.js'],
	        complete: function () {
	        	var $container = $('.hover_div');
				$container.imagesLoaded(function(){
				  	$container.masonry({
					    itemSelector : '.item',
					    columnWidth: '.item-sm'
				  	});
				});
	        }
	    }]);		
	}	
}

/**
 * @description Google map
 * @return 
 */
function k_map(){
	if ($('.k-map').length > 0) {
		Modernizr.load([{
	        load: 'assets/js/gmap.js',
	        complete: function () {
	            if ($('.k-map-contact-form .k-map').length > 0) {
					var map = new GMaps({
					  div: '.k-map',
					  lat: -12.043333,
					  lng: -77.028333
					});

					map.addMarker({
					  lat: -12.046307,
					  lng: -77.042531,
					  title: 'Lima'	  
					});
				}

				if ($('.k-widget-map .k-map').length > 0) {
					var map2 = new GMaps({
					  div: '.k-map',
					  lat: -12.046307,
					  lng: -77.028333
					});

					map2.addMarker({
					  lat: -12.046307,
					  lng: -77.042531,
					  title: 'Lima'	  
					});
				}
	        }
	    }]);
	}
}

/**
 * @description Masonry layout
 * @return 
 */
function k_masonry(){

	if ($('.masonry-container').length > 0) {
		Modernizr.load([{
            load: ['assets/js/masonry.js', 'assets/js/imagesloaded.js'],
            complete: function () {
                if ($('.list-blog-post-masonry').length > 0) {
					var $container = $('.list-blog-post-masonry');
					$container.imagesLoaded(function(){
					  	$container.masonry({
						    itemSelector : '.item',
						    columnWidth: '.item'
					  	});
					});
				}

				if ($('.list-blog-post-timeline').length > 0) {
					var $container2 = $('.list-blog-post-timeline');
					var $grid = $container2.imagesLoaded(function(){
					  	$grid.masonry({
						    itemSelector : '.item',
						    columnWidth: '.item'
					  	});
					});		

					$grid.on('layoutComplete', onLayout);

					$('.k-timeline .read-more').click(function(event){
						event.preventDefault();
						var html = '<li class=\"item\">\n <div class=\"item-inner\">\n  <div class=\"item-thumb\">\n  <a href=\"#\"><img src=\"assets\/imgs\/37.png\" alt=\"\"><\/a>\n  <div class=\"item-metadata k-item-metadata\">\n  <span class=\"item-metadata-time \">\n                                            <span class=\"item-metadata-time-day\">8<\/span>\n                                            <span class=\"item-metadata-time-mon\">Feb<\/span>\n                                        <\/span>\n                                        <span class=\"item-metadata-comments\">\n                                            <span class=\"item-metadata-comments-count\">47<\/span>\n                                            <span class=\"item-metadata-comments-title\">Com<\/span>\n                                        <\/span>\n                                    <\/div>\n                                    <!'+'-- item-metadata --'+'>\n                                <\/div>\n                                <!'+'-- item thumb --'+'>\n                                <div class=\"item-content\">\n                                    <h4 class=\"item-title\"><a href=\"#\">New WordPress Theme<\/a><\/h4>\n                                    <div class=\"item-metadata-2\">\n                                        <span class=\"item-metadata-2-author\"><span>By<\/span> <a href=\"#\">admin<\/a><\/span>&nbsp;&nbsp;|&nbsp;&nbsp;\n                                        <span class=\"item-metadata-2-cat\"><span>In<\/span> <a href=\"#\">Articles<\/a><\/span>   \n                                    <\/div>\n                                    <p class=\"item-text-content\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s.<\/p>\n                                    <a href=\"#\" class=\"read-more read-more-border\">read more<\/a>\n                                <\/div>\n                            <\/div>\n                            <div class=\"item-line\"><\/div>\n                            \n                        <\/li>';
						
						var $elems = $( html);
						$grid.append( $elems).masonry('appended', $elems);		

					});
				}

				if ($('.k-widget-portfolio-filter .masonry-items').length > 0) {
					var $container3 = $('.k-widget-portfolio-filter .masonry-items');
					var masonryOptions = {
					  	itemSelector : '.item.show',
						columnWidth: '.item.show'
					};

					$container3.imagesLoaded(function(){
					  	var $grid = $container3.masonry( masonryOptions);
					  	var $filters = $('.k-widget-portfolio-filter .masonry-filter a');

						$filters.click(function(event){

							event.preventDefault();
							$filters.removeClass('active');
							$(this).addClass('active');
							var $filter_val = $(this).data('val');
							$container3.find('.item').each(function(){

								var $item_val = $(this).data('val').toString().split(',');   				
								var a = $item_val.indexOf($filter_val.toString()) > -1;

							    if ($filter_val == "*") {
									$(this).removeClass('hide');
									$(this).addClass('show');
								} else {
									if ( a == true) {
										$(this).removeClass('hide');
										$(this).addClass('show');	
									} else {
										$(this).removeClass('show');
										$(this).addClass('hide');
									}
								}								
							});

							$container3.masonry('layout');
						});

						
						$('.k-widget-portfolio-filter .read-more').click(function(event){
							event.preventDefault();
							var html = '<div class=\"item show col-md-4 col-sm-4 col-xs-6\" data-val=\"1,2,3\">\n    <div class=\"item-thumb\">\n        <img src=\"assets\/imgs\/24.png\" alt=\"\">\n        <div class=\"overlay\">\n            <div>\n                <a href=\"#\" class=\"fa fa-plus\"><\/a>\n                <a href=\"#\" class=\"fa fa-expand\"><\/a>\n            <\/div>\n        <\/div>\n    <\/div>\n    <h4 class=\"item-title\"><a href=\"#\">Lorem ipsum dolor sit.<\/a><\/h4>\n    <h5 class=\"item-cat\"><a href=\"#\">Lorem<\/a> \/ <a href=\"#\">ipsum<\/a><\/h5>\n<\/div>';
							var $elems = $( html);
							$grid.append( $elems).masonry('appended', $elems);
						});

					});
				}

				if ($('.k-list-product').length > 0) {
					var $container4 = $('.k-list-product');
					$container4.imagesLoaded(function(){
					  	$container4.masonry({
						    itemSelector : '.col-md-4',
						    columnWidth: '.col-md-4'
					  	});
					});
				}
            }
        }]);
	}		
}
function onLayout() {	
	var $container = $('.list-blog-post-timeline');
	$container.find('.item').each(function(){
		var $this = $(this);
		var position = $(this).position();
		if (position.left == 0) {
			$this.removeClass('item-right').addClass('item-left');
		} else {
			$this.removeClass('item-left').addClass('item-right');
		}
	});
}


/**
 * @description Quantity product
 * @return 
 */
function k_quantity(){
	$(".entry-product .quantity .qty").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            (e.keyCode == 65 && e.ctrlKey === true) ||
            (e.keyCode == 67 && e.ctrlKey === true) ||
            (e.keyCode == 88 && e.ctrlKey === true) ||
            (e.keyCode >= 35 && e.keyCode <= 39)) 
        {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

	$('.entry-product .quantity .qty-minus').click(function(){
		var $this = $(this).parent().find('.qty');
		var $old_val = parseInt($this.val());
		var $new_val = $old_val - 1;
		if ($new_val < 1) {
			$new_val = 1;			
		}
		$this.val($new_val);
		
	});

	$('.entry-product .quantity .qty-plus').click(function(){
		var $this = $(this).parent().find('.qty');
		var $old_val = parseInt($this.val());
		var $new_val = $old_val + 1;
		$this.val($new_val);
	});
}

/**
 * @description sync carousel
 * @return 
 */
function k_sync_carousel(){

	var sync1 = $("#sync1");
  	var sync2 = $("#sync2");
 	
 	if (sync1.length > 0) {
 		Modernizr.load([{
            load: ['assets/js/owl.carousel.min.js', 'assets/js/elevateZoom.min.js'],
            complete: function () {
                sync1.owlCarousel({
				    singleItem : true,
				    slideSpeed : 500,
				    pagination:false,
				    responsiveRefreshRate : 200,
				    autoHeight:true,
				    addClassActive:true,
				    lazyLoad : true,
				    afterInit:function(){
				    	k_zoom();
				    },
				    afterAction:function(){
				    	var current = this.currentItem;
					    $("#sync2")
					      .find(".owl-item")
					      .removeClass("synced")
					      .eq(current)
					      .addClass("synced")
					    if($("#sync2").data("owlCarousel") !== undefined){
					      center(current)
					    }
				    	$('.zoomContainer').remove();	 
				    	k_zoom();  	
				    }
			  	});
			 
			  	sync2.owlCarousel({
				    items : 3,
				    responsiveRefreshRate : 100,
				    pagination:false,
					navigation:true,
					lazyLoad : true,
					navigationText:['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
				    afterInit : function(el){
				      el.find(".owl-item").eq(0).addClass("synced");
				    }
			  	});

			  	$("#sync2").on("click", ".owl-item", function(e){
				    e.preventDefault();
				    var number = $(this).data("owlItem");
				    sync1.trigger("owl.goTo",number);
			  	});
            }
        }]);
 	}
}
function syncPosition(el){
    var current = this.currentItem;
    $("#sync2")
      .find(".owl-item")
      .removeClass("synced")
      .eq(current)
      .addClass("synced")
    if($("#sync2").data("owlCarousel") !== undefined){
      center(current)
    }
}
function center(number){
    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for(var i in sync2visible){
      if(num === sync2visible[i]){
        var found = true;
      }
    }
 
    if(found===false){
      if(num>sync2visible[sync2visible.length-1]){
        sync2.trigger("owl.goTo", num - sync2visible.length+2)
      }else{
        if(num - 1 === -1){
          num = 0;
        }
        sync2.trigger("owl.goTo", num);
      }
    } else if(num === sync2visible[sync2visible.length-1]){
      sync2.trigger("owl.goTo", sync2visible[1])
    } else if(num === sync2visible[0]){
      sync2.trigger("owl.goTo", num-1)
    }    
}
function k_zoom(){
	setTimeout(function(){
		$("#sync1").find('.active img').elevateZoom({
    		responsive:true,
    		scrollZoom:true,
    		zoomWindowWidth:500,
    		zoomWindowHeight:500,
    		borderSize:1,
    		borderColour:'#e5e5e5',
    		lensColour:'#0c86c6',
    		lensOpacity:0.6,
    		zoomWindowPosition: "product-zoom"
    	});
	},500);	
}

/**
 * @description Gallery image flick
 * @return 
 */
function k_flick(){
	if ($('.flickr-wrap ul').length > 0) {
		Modernizr.load([{
            load: ['assets/js/jflickrfeed.js', 'assets/js/imgliquid.js'],
            complete: function () {
                $('.flickr-wrap ul').jflickrfeed({
			        limit: 4,
			        qstrings: {
			            id: '78715597@N07'
			        },
			        itemTemplate: '<li class="flickr-badge-image">' + '<a target="blank" href="{{link}}" title="{{title}}" class="imgLiquid" style="width:79px; height:79px;">' + '<img src="{{image_m}}" alt="{{title}}"  />' + '<i class="fa fa-plus"></i></a>' + '</li>'
			    }, function(data) {
			        jQuery('.flickr-wrap .imgLiquid').imgLiquid();
			    });
            }
        }]);		   
    }
}

/**
 * @description Slider animation
 * @return 
 */
function k_slider_animation(){
	if ($('.k-widget-slider-animation').length > 0) {
		Modernizr.load([{
	        load: ['assets/js/fractionslider.min.js'],
	        complete: function () {
	            $('.slider').fractionSlider({
					'fullWidth': 			true,
					'controls': 			true, 
					'responsive': 			true,
					'dimensions': 			"1366,500",
				    'increase': 			false,
					'pauseOnHover': 		true,
					'autoChange': 			false,
					'slideEndAnimation': 	true
				});
	        }
	    }]);
	}
}

/**
 * @description Search bar
 * @return 
 */
function k_search_bar(){
	if ($('.sb-search').length > 0) {
		Modernizr.load([{
            load: ['assets/js/uisearch.js', 'assets/js/classie.js'],
            complete: function () {
                new UISearch( document.getElementById('sb-search'));
            }
        }]);    	
    }
}

/**
 * @description Validate form
 * @return 
 */
function k_validate(){
	if (jQuery('.k-map-contact-form form').length > 0 || jQuery('.k-widget-contact-form form').length > 0 || jQuery('.comment-form').length > 0) {
        Modernizr.load([{
            load: ['assets/js/form.js', 'assets/js/validate.js'],
            complete: function () {
                jQuery('.k-map-contact-form form, .k-widget-contact-form form, .comment-form').validate({
                    rules: {
                        name: {
                            required: true,
                            minlength: 4
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        subject: {
                            required: true,
                            minlength: 10
                        },
                        message: {
                            required: true,
                            minlength: 10
                        }
                    },
                    messages: {
                        name: {
                            required: kopa_variable.i18n.validate.name.REQUIRED,
                            minlength: jQuery.format(kopa_variable.i18n.validate.name.MINLENGTH)
                        },
                        email: {
                            required: kopa_variable.i18n.validate.email.REQUIRED,
                            email: kopa_variable.i18n.validate.email.EMAIL
                        },
                        subject: {
                            required: kopa_variable.i18n.validate.subject.REQUIRED,
                            minlength: jQuery.format(kopa_variable.i18n.validate.subject.MINLENGTH)
                        },
                        message: {
                            required: kopa_variable.i18n.validate.message.REQUIRED,
                            minlength: jQuery.format(kopa_variable.i18n.validate.message.MINLENGTH)
                        }
                    },
                    submitHandler: function (form) {
                        jQuery('.k-map-contact-form form input[type="submit"], .k-widget-contact-form input[type="submit"], .comment-form input[type="submit"]').attr("value", kopa_variable.i18n.validate.form.SENDING);
                        jQuery(form).ajaxSubmit({
                            success: function (responseText, statusText, xhr, $form) {
                                jQuery("#response").html(responseText).hide().slideDown("fast");
                                jQuery('.k-map-contact-form form input[type="submit"], .k-widget-contact-form input[type="submit"], .comment-form input[type="submit"]').attr("value", kopa_variable.i18n.validate.form.SUBMIT);
                            }
                        });
                        return false;
                    }
                });
            }
        }]);
    }
}

/**
 * @description Responsive tabs 
 * @return 
 */
function k_tabs() {
	if ($('.k-tabs.style-3').length > 0) {		
    	responsive_tabs();
    	$( window ).resize( function () {
			k_responsive_tabs();
		});		
    }
}

function k_responsive_tabs(){    	
	$('.k-tabs.style-3').each(function(){
		var x = Math.floor((Math.random() * 100) + 1);
		var $htmlCollapse = '<div class="panel-group" id="accordion'+ x +'" role="tablist" aria-multiselectable="true">';
		var $parent = $(this);
		var $childrenNavTabs = $(this).find('.nav-tabs li');
		var $childrenNavContent = $(this).find('.tab-content .tab-pane');
		var windowWidth = $(window).width();

		if (windowWidth < 1024) {
    		
    		$childrenNavTabs.each(function(){
    			var $thisChildrenNavTabs = $(this);
    			$htmlCollapse += '<div class="panel panel-default">';
                $htmlCollapse += '<div class="panel-heading" role="tab" id="heading'+ $thisChildrenNavTabs.find('a').attr('aria-controls') +'">';
                $htmlCollapse += '<h4 class="panel-title">';

                if ($thisChildrenNavTabs.hasClass('active')) {
                	$htmlCollapse += '<a role="button" data-toggle="collapse" data-parent="#accordion'+ x +'" href="#collapse'+ $thisChildrenNavTabs.find('a').attr('aria-controls') +'" aria-expanded="true" aria-controls="collapse'+ $thisChildrenNavTabs.find('a').attr('aria-controls') +'">';
                } else {
                	$htmlCollapse += '<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion'+ x +'" href="#collapse'+ $thisChildrenNavTabs.find('a').attr('aria-controls') +'" aria-expanded="true" aria-controls="collapse'+ $thisChildrenNavTabs.find('a').attr('aria-controls') +'">';
                }

                $htmlCollapse += $(this).find('a').html();
                $htmlCollapse += '</a>';
                $htmlCollapse += '</h4>';
                $htmlCollapse += '</div>';
                  
                $childrenNavContent.each(function(){
                	if ($thisChildrenNavTabs.find('a').attr('aria-controls') == $(this).attr('id')) {
                		if ($thisChildrenNavTabs.hasClass('active')) {
                			$htmlCollapse += '<div id="collapse'+ $thisChildrenNavTabs.find('a').attr('aria-controls') +'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading'+ $thisChildrenNavTabs.find('a').attr('aria-controls') +'">';
                		} else {
                			$htmlCollapse += '<div id="collapse'+ $thisChildrenNavTabs.find('a').attr('aria-controls') +'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'+ $thisChildrenNavTabs.find('a').attr('aria-controls') +'">';
                		}
                		
                        $htmlCollapse += '<div class="panel-body">';
                        $htmlCollapse += $(this).html();
                        $htmlCollapse += '</div>';
                        $htmlCollapse += '</div>';
                	}
                });                        

    			$htmlCollapse += '</div>';
    		});

			$htmlCollapse += '</div>';

			$parent.find('> .nav-tabs').hide();
			$parent.find('> .tab-content').hide();

			if ($parent.find('> .panel-group').length < 1) {
				$parent.append($htmlCollapse);
			} else {
				$parent.find('> .panel-group').show();
			}

		} else {
			$parent.find('> .nav-tabs').show();
			$parent.find('> .tab-content').show();
			if ($parent.find('> .panel-group').length > 0) {
				$parent.find('> .panel-group').hide();
			}
		}
	});
}

/**
 * @description count up, progress bar animation 
 * @return 
 */
function k_scroll_animation() {
	Modernizr.load([{
        load: 'assets/js/countUp.js',
        complete: function () {
			$(window).scroll(function() {
				var $scrollTop = $(this);
				$('.k-widget-number').each(function(){
					var $this = $(this);
					var hT = $this.offset().top,
				       hH = $this.outerHeight(),
				       wH = $(window).height(),
				       wS = $scrollTop.scrollTop();
				   	if (wS > (hT+hH-wH)){
				    	k_count ();
				   	}
				});
			   
			});

			$(window).scroll(function() {
				var $scrollTop = $(this);
				$('.k-widget-progress').each(function(){
					var $this = $(this);
					var hT = $this.offset().top,
				       hH = $this.outerHeight(),
				       wH = $(window).height(),
				       wS = $scrollTop.scrollTop();
				   	if (wS > (hT+hH-wH)){
				    	k_progress_animation();
				   	}
				});
			   
			});
        }
    }]);
}

function k_count () {
	$('.k-widget-number .item').each(function(){
		var $this = $(this).find('.item-number');
		var $id = $this.attr("id");
		var $number = $this.data("number");
		if ($number > $this.text() && $id != null) {
			var $count = new CountUp($id, 0, $number,0 ,2);
			$count.start();
		};		
	});
}

function k_progress_animation (){
	$('.k-widget-progress .k-progress').each(function(){
		var $progress_bar = $(this).find('.progress-bar');
		var $this = $(this).find('.progress-bar  i');
		var $id = $this.attr("id");
		var $number = $this.data("number");

		if ($number > $this.text() && $id != null) {
			var $count = new CountUp($id, 0, $number,0 ,2);
			$count.start(function() {
				var myWidth = $this.text();				
			});
		};		
		$progress_bar.width($number + '%');
	});
}